import React, { Component } from "react";
import Counter from "./Counter";

class Counters extends Component {
  render() {
    console.log("Counters-Rendered");
    const {
      counters,
      onReset,
      onDelete,
      onIncrement,
      onDecrement,
    } = this.props;

    return (
      <>
        <button onClick={onReset} className="btn btn-primary btn-sm m-2">
          Reset
        </button>
        {counters.map((counter) => (
          <Counter
            key={counter.id}
            onDelete={onDelete}
            onIncrement={onIncrement}
            onDecrement={onDecrement}
            counter={counter}
            selected
          />
        ))}
      </>
    );
  }
}

export default Counters;
