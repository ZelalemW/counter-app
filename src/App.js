import React, { Component } from "react";
import { v4 as uuid } from "uuid";
// import axios from 'axios';
import "./App.css";
import NavBar from "./components/Navbar";
import Counters from "./components/Counters";

class App extends Component {
  state = {
    counters: [
      { id: uuid(), value: 0 },
      { id: uuid(), value: 4 },
      { id: uuid(), value: 0 },
      { id: uuid(), value: 0 },
    ],
  };

  constructor() {
    super();
    console.log("App - constructor called"); //called only once - when an instance is created
    //opportunity to set initial state as follows:
    //this.state = this.props.stuff // props has to be passed to the constructor and to the super() otherwise, it will be unavailable here
    //but the following will result error
    //this.setState() -- because this can only be called when the component is rendered in the DOM
  }

  componentDidMount() {
    // after the component is rendered on DOM.
    // perfect place to call API call
    console.log("App Mounted");
    // const api = "https://jsonplaceholder.typicode.com/todos?_limit=10";
    // axios.get(api)
    //       .then(res=>console.log(res.data));
    
  }

  handleDelete = (counterId) => {
    const counters = this.state.counters.filter((x) => x.id !== counterId);
    this.setState({ counters });
  };

  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index].value++;
    this.setState({ counters });
  };

  handleDecrement = (counter) => {
    const counters = this.state.counters.map((c) => {
      if (c.id === counter.id && c.value > 0) {
        c.value = c.value - 1;
      }

      return c;
    });

    this.setState({ counters });
  };

  handleReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });

    this.setState({ counters });
  };

  render() {
    console.log("App-Rendered");
    return (
      <>
        <NavBar
          totalCounters={this.state.counters.filter((c) => c.value > 0).length}
        />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDecrement={this.handleDecrement}
            onDelete={this.handleDelete}
          />
        </main>
      </>
    );
  }
}

export default App;
